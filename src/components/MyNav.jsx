import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults}) => {
    
    // const searchType = "videos"
    return (<Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
        </Nav>
        <SearchBar onResults={onResults} />
    </Navbar>);
}

export default MyNav;