import React, {useState} from 'react';
import {Button, Form, FormControl} from "react-bootstrap";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import api from '../lib/api';
import {useHistory} from "react-router-dom";

const SearchBar = ({onResults}) => {
    const [searchType, setSearchType] = useState('video');
    const [typed, setTyped] = useState('');
    const history = useHistory();

    const search = async () => {
        console.log("seach type", searchType)
        const resp = await api.get('/search', {
            params: {
                q: typed,
                part: 'snippet',
                maxResults: 10,
                type: searchType
            }
        });
        console.log('Received', resp.data.items);
        onResults(resp.data.items.map(i => ({...i, type: searchType})));
        history.push(`/search/${typed}`);
    };

    return (<Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2"
                     value={typed}
                     onChange={event => setTyped(event.target.value)}
        />
        <ButtonGroup toggle className="mr-sm-2">
          <ToggleButton
            type="radio"
            variant="secondary"
            name="videos-radio"
            value="video"
            checked={searchType === "video"}
            onChange={() => setSearchType("video")}
          >
            Videos
          </ToggleButton>
          <ToggleButton
            type="radio"
            variant="secondary"
            name="channels-radio"
            value="channel"
            checked={searchType === "channel"}
            onChange={() => setSearchType("channel")}
          >
            Channels
          </ToggleButton>
      </ButtonGroup>
        <Button variant="outline-info" onClick={search}>Search</Button>
    </Form>);
}

export default SearchBar;