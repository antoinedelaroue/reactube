import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Video from "./Video";
import api from '../lib/api';
import { useHistory } from "react-router-dom";

const DetailedChannel = ({ id, snippet, selectVideo }) => {
    const [videos, setVideos] = useState([])
    const { title, description } = snippet;
    const history = useHistory();

    useEffect(()=>{
        fetchVideos()
    }, [id])

    const fetchVideos = async () => {
        const resp = await api.get('/search', {
            params: {
                channelId: id,
                part: 'snippet',
                maxResults: 10,
                type: "video"
            }
        });
        console.log('Received channel videos', resp.data.items);
        setVideos(resp.data.items);
    };

    return (<Card style={{ width: '100%' }}>
        <Card.Body>
            <Button variant="danger" style={{ float: 'right' }} onClick={history.goBack}>Retour</Button>
            <Row>
                <Col>
                    <h2>{title}</h2>
                    <Card.Text dangerouslySetInnerHTML={{ __html: description.replace(/\n/g, '<br/>') }} />
                </Col>
            </Row>
            <Row>
                {videos.map(i => {
                    const {
                        title, description, thumbnails,
                        channelTitle, publishTime
                    } = i.snippet;

                    return (<Video
                        videoId={i.id.videoId}
                        thumbnail={thumbnails.high}
                        description={description}
                        channelTitle={channelTitle}
                        publishTime={publishTime}
                        title={title}
                        selectVideo={selectVideo} />);
                })}
            </Row>
        </Card.Body>
    </Card>);
}

export default DetailedChannel;