import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import Channel from "./components/Channel";
import DetailedVideo from "./components/DetailedVideo";
import DetailedChannel from "./components/DetailedChannel";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [searchItems, setSearchItems] = useState([]);
    const [selected, select] = useState({});

    const seleect = (ok)=>{
        console.log("selected",ok)
        console.log(selected.snippet)
        select(ok)
    }

    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={setSearchItems}/>

                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selected.id}
                                       snippet={selected.snippet} player={selected.player}
                                       statistics={selected.statistics} />
                    </Route>
                    <Route path="/channels/:channelId">
                        <DetailedChannel id={selected.id}
                                       snippet={selected.snippet} player={selected.player}
                                       statistics={selected.statistics}
                                       selectVideo={seleect} />
                    </Route>
                    <Route path="/search/:search">
                        <>
                            {searchItems.map(i => {
                                if(i.type === "video"){
                                    const {
                                        title, description, thumbnails,
                                        channelTitle, publishTime
                                    } = i.snippet;
    
                                    return (<Video
                                        videoId={i.id.videoId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectVideo={select}/>);
                                } else{
                                    const {
                                        title, description, thumbnails
                                    } = i.snippet;
    
                                    return (<Channel
                                        channelId={i.id.channelId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        title={title}
                                        selectChannel={select}/>);
                                }
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
